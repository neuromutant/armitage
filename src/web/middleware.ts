import {parse_schema, out_err, out_success} from '../helpers/out';

export const schema_middleware = (schema) => (req, res, next) => {
  if (schema && req.method === 'POST') {
    const { err } = parse_schema(req.body, schema)();
    if (err) {
      return res.send({err:{message:err.details[0].message}, success:null});
    }
  }
  return next();
};

export const role_middleware = (roles) => (req, res, next) => {
  if(req.user && req.user.role && roles.includes(req.user.role)) {
    return next();
  }
  return res.end({err:'role provided is not sufficient'});
};
  
export const action_middleware = (actions) => (req, res, next) => {
  if(req.user && req.user.can) {
    let result = true;
    for(const action of actions) {
      if(!req.user.can[action]) {
        result = false;
      }
    }
    if(result) {
      return next();
    }
  }
  return res.end({ err: 'user has no rights to perform this action' });
};