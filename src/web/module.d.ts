
interface err {
  err:any,
  success:null
}
interface success {
  err:null,
  success:any
}

declare interface i_methods {
    out_err(any):err, 
    out_success(any):success
    render(data?, options?, override?)
    res:any
  }

  interface controller_opts {
    body:any,
    user: any,
    params:any
  }

type controller = (options: controller_opts, methods:i_methods) => any