
export const create_route = (app, state, method: string) => (url: string, controller: controller) => {
  const middleware      = state.middleware || [];
  const methods:i_methods = state.methods;
  app[method.toLowerCase()](
    state.url + url, 
    middleware,
    async (req, res, next) => {
      methods.res = res;
      return controller(req, methods);
    });
}; 

