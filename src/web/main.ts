import {parse_schema, out_err, out_success}                    from '../helpers/out';
import {create_route}                                          from './web';
import {schema_middleware, role_middleware, action_middleware} from './middleware';

module.exports = {
  Armitage:(app, global_options={}, checkToken=() => true) => {
    return {
      namespace:(url:string, opts={}) => {
        if(!opts) { opts = {} }
        if(url && !url.startsWith('/')) {
          url = '/' + url;
        }
        const state = {
          name: '',
          params:[],
          view: false,
          socket: false,
          namespace_url: url,
          middleware: [],
          schema: {},
          url,
          shared_state:{},
          render:'',
          methods:{
            out_err, 
            out_success,
          },
          ...opts,
        };
        const funs = {
        // Descriptional
          name(name) {
            state.name = name;
            return funs;
          },
          // Descriptional
          params(...args) {
            state.params = args;
            return funs;
          },
          state(o, ttl?) {
            state.shared_state = {...o, ttl:ttl|0};
            return funs;
          },
          socket() {
            state.socket = true;
          },
          get(url, controller:controller) {
            funs.clear();
            create_route(app,state, 'get')(url, controller);
            return funs;
          },
          post(url, controller:controller) {
            funs.clear();
            create_route(app,state, 'post')(url, controller);
            return funs;
          },
          patch(url, controller:controller) {
            funs.clear();
            create_route(app,state, 'patch')(url, controller);
            return funs;
          },
          delete(url, controller:controller) {
            funs.clear();      
            create_route(app,state, 'delete')(url, controller);
            return funs;
          },
          middleware(mdl) {
            if(Array.isArray(mdl)) {
              state.middleware = mdl;
            } else {
              state.middleware = [mdl];
            }
            return funs;
          },
          schema(o) {
            state.middleware.push(schema_middleware(o));
            return funs;
          },
          view() {
            state.view = true;
            return funs;
          },
          role(...roles) {
            if(roles && roles.length) {
              if(!state.middleware.includes(checkToken)) {
                state.middleware.push(checkToken);
              }
              state.middleware.push(role_middleware(roles));
            }
            return funs;
          },
          can(...actions) {
            if(!state.middleware.includes(checkToken)) {
              if(actions && actions.length) {
                state.middleware.push(checkToken);
              }
              state.middleware.push(action_middleware(actions));
            }
            return funs;
          },
          clear() {
            state.schema = {};
            state.middleware = [];
            state.view = null;
            return funs;
          },
        };
        return funs;
      },
      listen
  
    };
  }
};
  