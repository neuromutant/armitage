
const express      = require('express');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const expressVue   = require('express-vue');
const app          = express();

app.use(bodyParser.json());
app.use(cookieParser());

const listen = (port=3030) => {
  expressVue.use(app, {
    pagesPath:__dirname + '/views',
    head: {
      title: 'Hello this is a global title',
      styles: [
        { style: 'https://assets.ubuntu.com/v1/vanilla-framework-version-2.1.0.min.css' }
      ]
    },
  }).then(() => {
    app.listen(3030);
  });
};
