import fs from 'fs';

const templates = {
  get(name,url, params) {
    return `   
        ${name}{
            return axios.get('${url}' ${params.join()})
        }`;
  },
  post(name, url, params) {
    return `   
    ${name}{
        return axios.post('${url}' ${params.join()})
    }`;
  },
  patch(name, url, params) {
    return `   
    ${name}{
        return axios.patch('${url}' ${params.join()})
    }`;
  },
  delete(name, url, params) {
    return `   
    ${name}{
        return axios.delete('${url}' ${params.join()})
    }`;
  }
};

const namespace = (namespace, route_str) => {
  return `
    export const ${namespace} {
      ${route_str}
    }
  `;
};

export const generate_namespace = (namespace_name, routes) => {
  let route_str = ``;
  for(const route of routes) {
    route_str += templates[route.url](route.name, route.url, route.params);
  }
  const result = namespace(namespace_name, route_str);
  fs.writeFile(__dirname + `/generator/generated/${namespace_name}.js`, result, err => {
    if (err) {
      console.error(err);
      return;
    }
  });
  
  return result; 
};
