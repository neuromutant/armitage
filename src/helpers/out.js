const Joi = require('@hapi/joi');
export const out = (err = null, success = null) => ({
    err,
    success
});
export const out_err = (err) => out(err, null);
export const out_success = (result) => out(null, result);
export const parse_in = ({ err, success }) => (di_err = out_err, di_success = out_success) => {
    if (err)
        return di_err(err);
    if (success)
        return di_success(success);
};
export const joi_parse_schema = (obj, schema) => {
    const result = Joi.validate(obj, schema);
    if (result.error === null) {
        return out_success(obj);
    }
    else {
        return out_err(result.error);
    }
};
export const parse_schema = (o, schema) => {
    if (!o || !schema) {
        return out_err({ message: 'no schema or object specified' });
    }
    return parse_in(joi_parse_schema(o, schema));
};
